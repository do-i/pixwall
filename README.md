# pixwall

PixWall is a Raspberry Pi backed picture slide show on kiosku display. 

### Install Steps

##### Supported hardware

PixWall works on following Raspberry Pi

* Raspberry Pi 3 Model B+
* Raspberry Pi Zero W
* Raspberry Pi Model 3 A+

Note: If setting up wifi is pain, build on B+ port the SD card onto Zero or A+.

##### Install Raspbian

**Download**

    wget -O 2019-04-08-raspbian-stretch-lite.zip https://downloads.raspberrypi.org/raspbian_full_latest

**Unzip**

    unzip 2019-04-08-raspbian-stretch-lite.zip

**Copy to micro SD card**

    ./more_dd.sh /dev/sdx 2019-04-08-raspbian-stretch-lite.zip 

Note: destination device path needs to be carefully chosen (use `sudo fdisk -l` to determine correct device path to SD card)

##### Enable sshd

create ssh file in /boot or run sshd service via sudo.


##### Install slide

[lightweight slideshow project](https://github.com/NautiluX/slide)

    wget https://github.com/NautiluX/slide/releases/download/v0.9.0/slide_pi_stretch_0.9.0.tar.gz
    tar xf slide_pi_stretch_0.9.0.tar.gz
    mv slide_0.9.0/slide /usr/local/bin/

##### Install dependency package
    sudo apt install qt5-default

##### Run slide remotely (via ssh)

    DISPLAY=:0.0 slide -p /home/pi/pixwall/jpgs

Note: This is useful white testing via ssh from remote machine.

##### Auto play upon boot

    mkdir -p /home/pi/.config/lxsession/LXDE-pi/
    vi /home/pi/.config/lxsession/LXDE-pi/autostart

Then, add the following content

```
@xset s noblank 
@xset s off 
@xset -dpms
@slide -t 5 -o 187 -p /home/pi/pixwall/jpgs
```

##### Disable blanking screen after 10 minutes

Add the following to `/etc/lightdm/lightdm.conf` and reboot rpi.

```
[SeatDefaults]
xserver-command=X -s 0 -dpms
```

##### Q & A

Q. There is an inch black space around the screen.

A. Update config.txt with 
    
    sudo vi /boot/config.txt

    # set 1 to disable_overscan
    disable_overscan=1


### Reference

https://opensource.com/article/19/2/wifi-picture-frame-raspberry-pi